#define _USE_MATH_DEFINES //desfines pi up to 50 digits
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <iomanip>


using namespace std;
int main();
void HarmonicSum(int power);
void HarmonicSum2(int power);
void Calcualtion2(int N,float x);
void Calculation3(int N,float x);
void Update2(double aprox,double exact, double M);
void aproxIntegrationCalc(int stoppingPower);
void aproxIntegrationSimpson(int stoppingPower);
void Update(int k, double count, double sum);


int main() {
	bool Exit = false;
	int power;
	float x;
	int question = 0;
	
	while (!Exit) {
		
		cout << "[1] Question 1 \t [2] Question 2 \t [3] Question 3 [4] Exit \n enter the Question number you would like to run: ";

		cin >> question;
		switch (question) {
		case 1:
			cout << "Question 1 " << endl;
			cout << " input the power: ";
			cin >> power;
			cout << "\n";
			HarmonicSum(power);
			HarmonicSum2(power);
			break;
		case 2:
			cout << "Question 2" << endl;
			cout << "enter an x: " ;
			cin >> x;
			cout << "\n number of times to perform calculation: ";
			cin >> power;
			cout << "\n";
			Calcualtion2(power,x);
			cout << "Pick an x between o and 1: ";
			cin >> x;
			cout << "\n";
			Calculation3(power, x);
			break;
		case 3:
			cout << "Question 3" << endl;
			cout << "This programs precision of pi: "<<setprecision(50)<< M_PI << endl;
			cout << "Enter a Power to stop calculating at: ";
			cin >> power;
			aproxIntegrationCalc(power);
			aproxIntegrationSimpson(power);
			break;
		case 4:
			Exit = true;
			cout << "Exiting" << endl;
			break;
		default: 
			cout << "wrong input" << endl;
			break;
		}
	}
	
	
	system("pause");
	return 0;
}

void HarmonicSum(int power) {
	double c = 0;
	double Sum = 0;
	int k = 0;
	while (c<pow(10,power)) {//loops until a certain amount of calculations have been completed
		c++;//counts the number of calculations done 

		Sum = Sum + (1 / c) ;
		
		if (c==pow(10,k+1)){
			k++;//k is the power tha has been calculated
			Update(k,c, Sum);
		}
	}
}
//basically the same as above except the sum being calculated is different
void HarmonicSum2(int power) {
	double c = 0;
	double Sum = 0;
	int k = 0;
	while (c < pow(10, power)) {
		c++;
		Sum = Sum + (1 / pow(3,c));
		
		if (c == pow(10, k+1)) {
			
			k++;
			Update(k,c, Sum);
		}
		
	}
}

void Update(int k,double count, double sum){
	//displayes the power and number of calculations completed along with the sum that was calcualted
	cout << "\n=============================" << endl;
	cout << "Power: "<< k <<" Calcualtions: " << count << endl;
	
	cout << setprecision(20);
	cout << " Sum: " << sum << endl;
	cout << "time: N/A" << endl;

}
//recursive method 
void Calcualtion2(int N,float x){
	double fx = 0;
	if (N == 1) {
		fx = (2 * pow(x, 3) + 5) / 3 * pow(x, 2);
		cout << "============================" << endl;
		cout << "X: "<< setprecision(3)<< x <<" calculation#: " << N <<endl;
		cout << "f(x) = "<<setprecision(10)<< fx << endl;
	}
	else {
	Calcualtion2(N - 1, x);
	fx = (2 * pow(pow(x, N), 3) + 5) / 3 * pow(pow(x, N), 2);
	cout << "============================" << endl;
	cout << "X: " << setprecision(3) << x << " calculation#: " << N << endl;
	cout << "f(x) = " << setprecision(10) << fx << endl;
	}


}
//recursive method 
void Calculation3(int N, float x) {
	double fx = 0;
	double c = 0.95;
	if (N == 1) {
		cout << "\n===========================" << endl;
		cout << "f(x) = c * x *(1 - x)";
		cout << "Calcualtion#: " << N << endl;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
		c = 2.0;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
		c = 3.6;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
		c = 3.98;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
	}
	else {
		Calculation3(N - 1, x);
		cout << "\n===========================" << endl;
		cout << "f(x) = c * x *(1 - x)";
		cout << "Calcualtion#: " << N << endl;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
		c = 2.0;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
		c = 3.6;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
		c = 3.98;
		cout << "c: " << setprecision(4) << c << " x: " << setprecision(2) << x;
		fx = (c*pow(x, N))*(1 - pow(x, N));
		cout << " result: " << setprecision(25) << fx << "\n -------------------------" << endl;
	}
}

void Update2(double aprox, double exact, double M) {

	cout << "\n=========================" << endl;
	cout << "Exact Integral: " << exact << endl;
	cout << "Aprox Integral: " << aprox << endl;
	cout << "Difference: " << abs(aprox - exact) << endl;
	cout << "M Value: " << M << endl;

}

void aproxIntegrationCalc(int stoppingPower) {
	cout << "\nXXXXXXXXXXXXXXXXXXX APROXIMATE INTEGRAL XXXXXXXXXXXXXXXXXXXXX" << endl;
	double EXACT_INTEGRAL = 2 / M_PI;
	double M = 2;
	double h = 1;
	int p = 1;
	double _SUM = 0;
	while (M != pow(2, stoppingPower)) {
		h = 1 / M;
		for (int k = 0; k < M - 1; k++) {
			_SUM = _SUM + sin(M_PI*(k*h));
		}
		_SUM = _SUM * h;
		Update2(_SUM, EXACT_INTEGRAL, M);
		p++;
		M = pow(2, p);

	}

}

void aproxIntegrationSimpson(int stoppingPower) {
	cout << "\nXXXXXXXXXXXXXXXXXXX APROXIMATE INTEGRAL SIMPSON'S RULE XXXXXXXXXXXXXXXXXXXXX" << endl;
	double EXACT_INTEGRAL = 2 / M_PI;
	double M = 2;
	double h = 1;
	int p = 1;
	double _SUM = 0;
	double r;
	while (M != pow(2, stoppingPower)) {
		h = 1 / M;
		for (int k = 0; k < M; k++) {
			r = sin(M_PI*(k*h));
			if (k % 2 == 0 && (k != 0 || k != M)) {
				_SUM = _SUM + r * 2;
			}
			else if (k == 0 || k == M){
				_SUM = _SUM + r;
			}else {
				_SUM = _SUM + r * 4;
			}
			
		}
		_SUM = _SUM * h/3;
		Update2(_SUM, EXACT_INTEGRAL, M);
		p++;
		M = pow(2, p);

	}

}
